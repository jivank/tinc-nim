import unittest, ospaths, tinc, os, tables, marshal, sugar, sequtils, options

suite "test suite for tinc":
  let
      tincContext = tinc.findTinc().get()
      network1 = tincContext.initNetwork("testnetwork1","10.0.0.1")
      network2 = tincContext.initNetwork("testnetwork2","10.0.1.1")
  setup:
    discard
   

  test "test get networks":
    let networks = tincContext.getNetworks()
    let networkNames = lc[x.networkName | (x <- networks),string]
    check:
        networks.len == 2
        "testnetwork1" in networkNames
        "testnetwork2" in networkNames

  test "test get hosts":
    let networks = tincContext.getNetworks()
    check:
        networks.len == 2

  test "test get unused ip":
      let unusedIP = tincContext.getNetwork("testnetwork1").get.findUnusedIp()
      check unusedIP == "10.0.0.2"

  test "test unused subnet":
    let unusedSubnet = tincContext.findUnusedSubnet()
    check:
      "10.0.2.1" == unusedSubnet
  
  tincContext.removeNetwork("testnetwork1")
  tincContext.removeNetwork("testnetwork2")

