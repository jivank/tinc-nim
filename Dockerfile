FROM nimlang/nim:latest as builder
COPY src /opt
RUN nimble install rosencrantz -y
RUN cd /opt && nim c -d:release webapp.nim

FROM debian:buster
COPY --from=builder /opt /opt
RUN apt update && apt install tinc -y
WORKDIR /opt
ENTRYPOINT [ "/opt/webapp" ]

EXPOSE 8080