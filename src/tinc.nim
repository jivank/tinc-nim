# tinc
# Copyright Jivan Kulkarni
# tinc vpn wrapper
import os, sequtils, strutils, tables, options, sugar, sets, nativesockets, strformat, osproc, winvpn
type
    Tinc* = ref object
        path*: string
        exe: string
    TincNetwork* = ref object
        networkName*: string
        nodeName*: string
        port*: int
        path*: string
        connectTo*: Option[string]
        tap*: Option[int]
        device*: Option[string]
        intrface*: Option[string]
    TincHost* = ref object
        name*: string
        subnet*: string
        path*: string
        address*: Option[string]
        port*: Option[Port]

proc findTinc*(): Option[Tinc] =
    var tinc = Tinc()
    if system.hostOS == "linux":
        tinc.path = "/etc/tinc"
    if system.hostOS == "macosx":
        let
            osxDir1 = "/usr/local/etc/tinc"
            osxDir2 = "/opt/local/etc/tinc"
        if existsDir(osxDir1):
            tinc.path = osxDir1
        if existsDir(osxDir2):
            tinc.path = osxDir2
        if existsDir(osxDir1) and existsDir(osxDir2):
            echo(osxDir1 & " and " & osxDir2 & " both exist, going with latter")
    if system.hostOS == "windows":
        let
            winDir1 = r"C:\Program Files (x86)\tinc"
            winDir2 = r"C:\Program Files\tinc"
        if existsDir(winDir1):
            tinc.path = winDir1
        if existsDir(winDir2):
            tinc.path = winDir2
        if existsDir(winDir1) and existsDir(winDir2):
            echo(winDir1 & " and " & winDir2 & " both exist, going with latter")
        
    if system.hostOS == "linux" or system.hostOS == "macosx":
        tinc.exe = findExe("tincd")
    if system.hostOS == "windows":
        tinc.exe = joinPath(tinc.path,"tincd.exe")
        
    if existsDir(tinc.path) and existsFile(tinc.exe):
        return some(tinc)
    else:
        return none(Tinc)

proc readTincConf*(tincConf: string): Table[string,string] =
    var confTable = initTable[string,string]()
    for line in lines(tincConf):
        if line.contains("RSA"): break
        if line.len != 0:
            var cfgParts = line.split("=")
            if cfgParts.len < 2: continue
            confTable[cfgParts[0].strip()] = cfgParts[1].strip()
    return confTable

proc validateTincConf(networkName: string, conf: Table[string,string]) =
    var messages = newSeq[string]()
    if not conf.hasKey("Name"):
        messages.insert "Name not configured in network: " & networkName
    else:
        if conf["Name"].len == 0:
            messages.insert "Name is empty in network: " & networkName
    if messages.len > 0:
        raise newException(Exception,messages.join(sep="\n"))

proc validateTincHostConf(hostName: string, conf: Table[string,string]) =
    var messages = newSeq[string]()
    if not conf.hasKey("Subnet"):
        messages.insert "Subnet not configured in host: " & hostName
    if messages.len > 0:
        raise newException(Exception,messages.join(sep="\n"))

proc getHost*(path: string): TincHost =
    let
        tincHost = TincHost()
        tincHostConf = readTincConf(path)
    tincHost.name = path.split(DirSep)[^1]
    tincHost.path = path
    tincHost.name.validateTincHostConf tincHostConf
    tincHost.subnet = tincHostConf["Subnet"]
    tincHost.address = if tincHostConf.hasKey("Address"): tincHostConf["Address"].some else: none(string)
    return tincHost

proc getHost*(tincNetwork: TincNetwork, hostname: string, dir="hosts"): TincHost = getHost(tincNetwork.path/dir/hostname)

proc getConfiguredSubnet*(tincNetwork: TincNetwork): string = tincNetwork.getHost(tincNetwork.nodeName).subnet

proc getHosts*(tincNetwork: TincNetwork,dir="hosts"): seq[TincHost]  =
    var hosts = newSeq[TincHost]()
    for kind, path in walkDir(tincNetwork.path/dir):
        if kind == pcFile:
            var tincHost = getHost(path)
            hosts.insert tincHost
    return hosts

proc getPendingHosts*(tincNetwork: TincNetwork): seq[TincHost] = getHosts(tincNetwork,dir="pending")

proc getNetwork*(tinc: Tinc, name: string): Option[TincNetwork] =
    var 
        tincNetwork = TincNetwork()
        tincNetworkConf: Table[string,string]
    let path = tinc.path / name
    try:
        tincNetworkConf = readTincConf(path/"tinc.conf")
    except:
        return none(TincNetwork)
    tincNetwork.networkName = path.split(DirSep)[^1]
    tincNetwork.path = path
    tincNetwork.networkName.validateTincConf tincNetworkConf
    tincNetwork.nodeName = tincNetworkConf["Name"]
    tincNetwork.port = tincNetworkConf.getOrDefault("Port","655").parseInt
    tincNetwork.connectTo = if tincNetworkConf.hasKey("ConnectTo"): tincNetworkConf["ConnectTo"].some else: none(string)
    tincNetwork.device = if tincNetworkConf.hasKey("Device"): tincNetworkConf["Device"].some else: none(string)
    if system.hostOS == "macosx":
        tincNetwork.tap = tincNetwork.device.flatMap((x: string) => x.split("tap")[^1].parseInt.some)
    else:
        tincNetwork.tap = none(int)
    tincNetwork.intrface = if tincNetworkConf.hasKey("Interface"): tincNetworkConf["Interface"].some else: none(string)
    return tincNetwork.some

proc getNetworks*(tinc: Tinc): seq[TincNetwork]  =
    var networks = newSeq[Option[TincNetwork]]()
    for kind, path in walkDir(tinc.path):
        if kind == pcDir:
           let (_,networkName) = splitPath(path)
           var tincNetwork = tinc.getNetwork(networkName)
           networks.insert tincNetwork
    return networks.filter(x => x.isSome).map(x => x.get)



proc findUnusedIp*(tincNetwork: TincNetwork): string =
    var uniqueSubnets = initSet[int]()
    var unusedIpParts = tincNetwork.getHosts[0].subnet.split(".")
    for host in tincNetwork.getHosts:
        let address = host.subnet.split('/')[0]
        uniqueSubnets.incl parseInt(address.split(".")[3])
    for host in tincNetwork.getHosts(dir="pending"):
        let address = host.subnet.split('/')[0]
        uniqueSubnets.incl parseInt(address.split(".")[3])

    for i in 1..254:
        if i notin uniqueSubnets:
            unusedIpParts[3] = $i
            return unusedIpParts.join(sep=".")

proc findUnusedSubnet*(tinc: Tinc): string =
    var uniqueSubnets = initSet[int]()
    for network in tinc.getNetworks:
        for host in network.getHosts:
            let address = host.subnet.split('/')[0]
            uniqueSubnets.incl parseInt(address.split(".")[2])

    if uniqueSubnets.len == 0:
        return "10.0.0.1"

    for i in 0..254:
        if i notin uniqueSubnets:
            return "10.0.$#.1".format($i)

proc findUnusedPort(tinc: Tinc): int = 
    var ports = initSet[int]()
    for network in tinc.getNetworks:
        ports.incl network.port
    for i in 655..65535:
        if i notin ports:
            return i

proc findUnusedTapDevice(tinc: Tinc): int =
    var taps = initSet[int]()
    for network in tinc.getNetworks:
        if network.tap.isSome:
            taps.incl network.tap.get
    for i in 0..99:
        if i notin taps:
            return i

proc writeConfigFileForNetwork*(tincNetwork: TincNetwork) =
    let filePath = tincNetwork.path / "tinc.conf"
    var osAdapterLine = ""
    if tincNetwork.device.isSome:
        osAdapterLine = fmt"Device = {tincNetwork.device.get}"
    if tincNetwork.intrface.isSome:
        osAdapterLine = fmt"Interface = {tincNetwork.intrface.get}"
    if tincNetwork.connectTo.isSome:
        osAdapterLine = fmt"ConnectTo = {tincNetwork.connectTo.get}"

    var content = fmt"""
Name = {tincNetwork.nodeName}
Port = {tincNetwork.port}"""
    if osAdapterLine.len > 0:
        content &= "\n" & osAdapterLine
    filePath.writeFile(content)

proc removeNetwork*(tinc: Tinc, networkName: string) =
    removeDir tinc.path/networkName

proc writeHostFile*(tincHost: TincHost) = 
    let
        key = readFile(tincHost.path)
    var hostFileData = fmt"Subnet = {tincHost.subnet}" & "\n"
    if tincHost.address.isSome:
        hostFileData &= fmt"Address = {tincHost.address.get}" & "\n"
    if tincHost.port.isSome:
        hostFileData &= fmt"Port = {tincHost.port.get}" & "\n"
    hostFileData &= key
    tincHost.path.writeFile(hostFileData)
    
proc getHostnameValidForTinc*(): string = getHostname().replace("-","").replace(".","")

proc getHostsPath*(network: TincNetwork): string = network.path / "hosts"
proc getPendingPath*(network: TincNetwork): string = network.path / "pending"


proc approveClient*(network: TincNetwork, clientName: string): bool =
    let clientFile = network.getPendingPath / clientName
    try:
        moveFile(clientFile,network.getHostsPath)
        return true
    except OSError:
        return false
    


proc initNetwork*(tinc: Tinc, networkName: string, publicIp = "", connectTo = "", connectToPort = -1, subnet = ""): TincNetwork =
    let
        netPath = tinc.path/networkName
        hostname = getHostnameValidForTinc()
        subnet = if subnet.len > 0: subnet else: tinc.findUnusedSubnet() 
    createDir netPath
    createDir netPath/"pending"
    createDir netPath/"hosts"
    var tincNetwork = TincNetwork()
    tincNetwork.networkName = networkName
    tincNetwork.port = tinc.findUnusedPort
    tincNetwork.nodeName = hostname
    tincNetwork.path = netPath
    tincNetwork.connectTo = if connectTo.len > 0: connectTo.some else: none(string)

    if hostOS == "macosx":
        tincNetwork.tap = tinc.findUnusedTapDevice.some
        tincNetwork.device = "/dev/tap$#".format(tincNetwork.tap.get).some

    if hostOS != "windows":
        let
            tincUpConf = "ifconfig $INTERFACE " & subnet & " netmask 255.255.255.0"
            tincDownConf = """ifconfig $INTERFACE down"""
            tincUpPath = netpath / "tinc-up"
            tincDownPath = netPath / "tinc-down"

        writeFile(tincUpPath, tincUpConf)
        writeFile(tincDownPath, tincDownConf)
        os.setFilePermissions(tincUpPath,{fpUserExec,fpOthersExec})
        os.setFilePermissions(tincDownPath,{fpUserExec,fpOthersExec})

    else:
        discard setupAdapter(networkName, tinc.path)

    tincNetwork.writeConfigFileForNetwork()
    #generate key
    let 
        generateKeyOutput = execProcess(fmt"{tinc.exe} -n {networkName} -K4096")
        tincHost = TincHost()
    tincHost.name = hostname
    tincHost.path = netPath/"hosts"/hostname
    tincHost.subnet = subnet&"/32"
    tincHost.address = if publicIp.len > 0: publicIp.some else: none(string)
    tincHost.port = if connectToPort != -1: Port(connectToPort).some else: none(Port)
    writeHostFile(tincHost) 
    return tincNetwork

