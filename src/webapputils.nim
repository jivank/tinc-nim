import tinc, options, base64, json, rosencrantz, httpcore, strformat, os, httpclient, uri


proc sendJoinRequest*(tincContext: Tinc, networkName, nodeUrl: string): auto =
    var client = newHttpClient()
    let baseUrl = parseUri(nodeUrl) / "api" / "networks" / networkName / "join"
    let
        response = parseJson(client.getContent($baseUrl))
        subnet = response["subnet"].getStr
        hostNodeName = response["hostNodeName"].getStr
        publicKey = response["publicKey"].getStr.decode

    let localNetwork = tincContext.initNetwork(networkName,connectTo=hostNodeName, subnet=subnet)
    writeFile(localNetwork.getHostsPath/hostNodeName,publickey)
    client.headers = newHttpHeaders({ "Content-Type": "application/json" })
    let body = %*{
        "clientName": getHostnameValidforTinc(),
        "publicKey": localNetwork.getHost(localNetwork.nodeName).path.readFile.encode
    }
    try:
        let
            joinUrl = baseUrl / "join"
            response = client.request($joinUrl, httpMethod = HttpPost, body = $body)
    except:
        echo "Failed"
    complete(Http302,"",newHttpHeaders([("Location","/")]))


proc processApproveClient*(tincContext: Tinc, network, client: string): auto =
    var networkOption = tincContext.getNetwork(network)
    if networkOption.isNone:
        return complete(Http404,fmt"Network: {network}, not found")
    var
        host: TincHost
        tincNetwork = networkOption.get
    # TODO: getHost to return option
    try:
        host = tincNetwork.getHost(client,dir="pending")
    except:
        return complete(Http404,fmt"Client: {client}, not found")
    
    moveFile(host.path,tincNetwork.getHostsPath/client)
    complete(Http302,"",newHttpHeaders([("Location","/")]))



proc processClientJoin*(tincContext: Tinc, network, clientName, publicKey: string): auto =
    let publicKeyDecoded = publicKey.decode
    writeFile(tincContext.getNetwork(network).get().path / "pending" / clientName, publicKeyDecoded)
    return ok("Success")

proc infoForNetwork*(tincContext: Tinc, networkName: string): string=
    let
        network = tincContext.getNetwork(networkname).get
        response = %*
            {
            "hostNodeName" : network.nodeName,
            "subnet" : network.findUnusedIp(),
            "publicKey" : encode readFile(network.getHost(network.nodeName).path)
            }
    return $response