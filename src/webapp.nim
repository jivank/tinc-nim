import rosencrantz, asynchttpserver, strformat, asyncdispatch, os, uri, options, tinc, strtabs, webapputils, json, ospaths
import httpClient
include "main.tmpl"

var client = newHttpClient()
let
  tincContext = tinc.findTinc().get()
  ip = client.getContent("http://icanhazip.com")

const
  bulma = staticRead("static/bulma.min.css")
  fontawesome = staticRead("static/fa.all.js")

proc filterNonTrusted(): Handler =
  proc h(req: ref Request, ctx: Context): Future[Context] {.async.} =
    if req.hostname == "127.0.0.1":
      return ctx
    else:
      return ctx.reject()
  return h



let handler = logRequest("$1 $2") -> logResponse("$1 $2 - $5") -> (rosencrantz.get[
  (path("/bulma.min.css") ->
    contentType("text/css") ->
    ok(bulma)) ~
  (path("/all.js") ->
    contentType("text/javascript") ->
    ok(fontawesome))
] ~
rosencrantz.get[
  path("/")[
    filterNonTrusted() -> (
        contentType("text/html")[
          scope do:
            let networks = tincContext.getNetworks()
            ok(generateHTMLPage(networks,ip))]
     ) ~ complete(Http403, "not authorized")
  ]
 ] ~
  rosencrantz.post[
    path("/networks")[
        formBody(proc(form: StringTableRef): auto =
          discard tincContext.initNetwork(form["networkName"],form["publicIp"])
          complete(Http302,"",newHttpHeaders([("Location","/")])))]
  ] ~
  rosencrantz.post[
    pathChunk("/networks")[
      segment(proc(network: string): auto =
      pathChunk("/approve")[
        segment(proc(client: string): auto =
           tincContext.processApproveClient(network,client)
        )
      ] 
    )
  ] ~
  rosencrantz.post[
    pathChunk("/removeNetwork")[
      segment(proc(network: string): auto =
          let network = tincContext.getNetwork(network)
          if network.isNone:
            return complete(Http404,"")
          removeDir(network.get.path)
          complete(Http302,"",newHttpHeaders([("Location","/")]))
      )
    ]
  ] ~
  rosencrantz.post[
    path("/joinNetwork")[
          formBody(proc(form: StringTableRef): auto =
            var 
              networkName, nodeUrl: string
            networkName = form["networkName"] 
            nodeUrl = form["nodeUrl"]
            return tincContext.sendJoinRequest(networkName, nodeUrl))]
  ] ~
   pathChunk("/api/networks")[
    rosencrantz.get[
      segment(proc(network: string): auto =
        pathEnd("/join")[
          ok(tincContext.infoForNetwork(network))
        ] ~ ok(network))
   ] ~ rosencrantz.post[
      segment(proc(network: string): auto =
        pathEnd("/join")[
          jsonBody(proc(j: JsonNode): auto =
            tincContext.processClientJoin(network, j.getStr("clientName"), j.getStr("publicKey"))
          )])]]])


let server = newAsyncHttpServer()
waitFor server.serve(Port(getEnv("TINC_WEB_PORT","8080").parseInt), handler)
