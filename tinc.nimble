# Package

version       = "0.1.0"
author        = "Jivan Kulkarni"
description   = "tinc vpn wrapper"
license       = "GPLv3"
srcDir        = "src"

# Dependencies

requires "nim >= 0.18.0"
